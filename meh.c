#define _POSIX_C_SOURCE 200112L

#include <apr_pools.h>
#include <libcrange.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const size_t MAX_APR_ERR_MSG_SIZE = 1024;
const char *usage =
	"Usage: %s [-ceh] range_query\n\nOptions:\n\t-e - expand (one host per line\n\t-c - count\n\t-h - print this usage text\n\n";

// Return value must be freed
static char *concat_argv(int argc, char **argv, bool count) {
	int len = 0;
	char *ret = NULL;

	for (int i = 1; i < argc; i++)
		len += (strlen(argv[i]) + 1); // space

	if ((ret = calloc(1, len + 1)) == NULL)
		return NULL;

	for (int i = 1; i < argc; i++) {
		strncat(ret, argv[i], len);
		strncat(ret, " ", 1);
	}

	ret[len] = '\0';

	if (count) {
		int new_len = strlen(ret) + strlen("count()");
		char *new_query = malloc(new_len + 1);
		if (! new_query) {
			free(ret);
			return NULL;
		}

		if (snprintf(new_query, new_len + 1, "count(%s)", ret) != new_len) {
			free(ret);
			return NULL;
		}

		free(ret);
		ret = new_query;
	}

	return ret;
}

static const char **expand_arg(apr_pool_t *pool, char *query) {
	libcrange *lr = libcrange_new(pool, NULL);
	if (lr == NULL)
		return NULL;

	range_request *rr = range_expand(lr, pool, query);
	if (range_request_has_warnings(rr)) {
		fprintf(stderr, "%s\n", range_request_warnings(rr));
		return NULL;
	}

	return range_request_nodes(rr);
}

int main(int argc, char **argv) {
	bool count = false;
	bool expand = false;
	int ret = EXIT_SUCCESS;
	char opt;
	char* query = NULL;
	const char** results;

	while ((opt = getopt(argc, argv, "ceh")) != -1) {
		switch (opt) {
			case 'c':
				count = true;
				argv++; argc--;
				break;
			case 'e':
				expand = true;
				argv++; argc--;
				break;
			case 'h':
				fprintf(stderr, usage, basename(argv[0]));
				return EXIT_FAILURE;
			default:
				fprintf(stderr, "Invalid option: %c\n", optopt);
				return EXIT_FAILURE;
		}
	}

	if (argc == 1) {
		fprintf(stderr, usage, basename(argv[0]));
		return EXIT_FAILURE;
	}

	apr_pool_t *pool;
	apr_initialize();
	apr_status_t stat = apr_pool_create(&pool, NULL);

	if (stat != APR_SUCCESS) {
		char* err_message = calloc(sizeof(char), MAX_APR_ERR_MSG_SIZE);
		apr_strerror(stat, err_message, MAX_APR_ERR_MSG_SIZE);
		fprintf(stderr, "Could not initialize APR pool: %s\n", err_message);
		free(err_message);

		return EXIT_FAILURE;
	}

	if ((query = concat_argv(argc, argv, count)) == NULL) {
		fprintf(stderr, "Could not concatenate arguments\n");
		ret = EXIT_FAILURE;
		goto bad;
	}

	if ((results = expand_arg(pool, query)) == NULL) {
		fprintf(stderr, "Could not expand query %s\n", query);
		ret = EXIT_FAILURE;
		goto bad;
	}

	for (int i = 0; results[i] != NULL; i++)
		printf("%s%c", results[i], (expand || results[i+1] == NULL) ? '\n' : ',');

bad:
	if (query)
		free(query);
	query = NULL;

	apr_pool_clear(pool);
	apr_pool_destroy(pool);
	apr_terminate();

	return ret;
}

